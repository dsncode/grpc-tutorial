# gRPC Tutorial

## Introduction
on this basic tutorial, we will see how to create a server that can handle gRPC calls. in addition, we will create a client that will send a request to this server and receive a response. 
all will be done using golang and its ready to use libraries. let's get started!

### Required software
it is required to have the following tools

### **golang**
all this tutorial is done using golang. but I'm sure you have it installed on your pc already ;)

in addition, please install these 2 libraries on your machine
```
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u google.golang.org/grpc
```

### **protoc**
this tool is required to compile proto files. if you are on ubuntu/debian just run
```
apt install -y protobuf-compiler
protoc --version  # Ensure compiler version is 3+
```
for other OS, please follow this tutorial  https://grpc.io/docs/protoc-installation/

### **Server**
this code, can be found on [server](/server). to run it, please type 
```
go run main.go
```
this will start a gprc server and listen for requests on port 8000

### **Client**
this code is available on [client](/client). to run it, as always just type
```
go run main.go
```
this will send a request to the previously created server
