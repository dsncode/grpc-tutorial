package main

import (
	"context"
	"fmt"

	"log"
	"net"

	"gitlab.com/dsncode/grpc-tutorial/server/hellopb"
	"google.golang.org/grpc"
)

type server struct {
}

const PORT = 8000

func (*server) Hello(ctx context.Context, request *hellopb.HelloRequest) (*hellopb.HelloResponse, error) {
	log.Println("Got a request!")
	name := request.Name
	response := &hellopb.HelloResponse{
		Greeting: "Hello " + name,
	}
	log.Println("Return answer...")
	return response, nil
}

func main() {
	address := fmt.Sprintf("0.0.0.0:%d", PORT)
	lis, err := net.Listen("tcp", address)
	if err != nil {
		log.Fatalf("Error %v", err)
	}
	fmt.Printf("Server is listening on %v ...", address)

	s := grpc.NewServer()
	hellopb.RegisterHelloServiceServer(s, &server{})

	s.Serve(lis)
}
