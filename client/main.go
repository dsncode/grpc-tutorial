package main

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/dsncode/grpc-tutorial/client/hellopb"
	"google.golang.org/grpc"
)

func main() {
	fmt.Println("Hello client ...")

	opts := grpc.WithInsecure()
	cc, err := grpc.Dial("localhost:8000", opts)
	if err != nil {
		log.Fatal(err)
	}
	defer cc.Close()

	client := hellopb.NewHelloServiceClient(cc)
	request := &hellopb.HelloRequest{Name: "DsnCode!"}

	resp, _ := client.Hello(context.Background(), request)
	fmt.Printf("Receive response => [%v]\n", resp.Greeting)
}
